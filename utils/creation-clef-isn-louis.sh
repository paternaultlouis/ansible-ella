#! /usr/bin/env bash

set -e

echo "CREATION DE LA CLE LIVE - PERSISTANTE"
echo "-------------------------------------"
echo ""

function _pas_usb {
  echo "Le disque ne semble pas être une clé USB."
  echo "Arrêt."
  exit 1
}

function _usage {
  echo "--------------------------------------------------------------------------------"
  echo "creationcleV2.sh"
  echo ""
  echo "creationcleV2 /dev/DEVICE : Installe un système live Debian sur la clef donnée en argument."
  echo "creationcleV2 : Installe un système live Debian sur l'unique clef USB montée (erreur si aucune ou plusieurs clefs USB sont mountées)."
  exit 1
}

function verifie_clef_usb {
  # `exit 1` si l'argument n'est pas une clef USB.
  DEVICE=$1

  # Le fichier existe
  if [ ! -e ${DEVICE} ]
  then
    _pas_usb
  fi

  # Le fichier est une clef USB
  ID=$(find -L /dev/disk/by-id -samefile $DEVICE | head -n 1)
  if [ -z $ID ] || [ ${ID:0:19} != "/dev/disk/by-id/usb" ]
  then
    _pas_usb
  fi
}

function cherche_clef_usb {
  if (( $# == 0 ))
  then
    echo $(readlink -e $(find /dev/disk/by-id -type l -name 'usb-*' | sort | head -n 1))
  elif (( $# == 1 ))
  then
    echo $1
  else
    _usage
  fi
}

################################################################################
# Création d'un répertoire temporaire, et suppression automatique à la fin
TEMPDIR=$(mktemp -d --suffix cleflive)
function cleanup {
  echo "Suppression du répertoire temporaire $TEMPDIR."
  rm -fr $TEMPDIR
}
trap cleanup EXIT
################################################################################

DEVICE=$(cherche_clef_usb "$@")
verifie_clef_usb $DEVICE

echo "Disques présents sur ce PC :"
lsblk
echo "----------------------------"
echo "La clé suivante va être effacée."
lsblk --output NAME,LABEL,SIZE,MOUNTPOINT --noheadings --scsi ${DEVICE}
read -p "Tout est OK ? [oui/non] " reponse

if [ $reponse = 'oui' ]; then
 echo "Démontage de la clé ${DEVICE}"
 umount  ${DEVICE}* || true

 echo ""
 echo "Copie de l'iso sur la clé ${DEVICE}"
 dd if=~prof/liveISN-debian-amd64.hybrid.iso of=${DEVICE} bs=2048 status=progress
 echo "Sync…"
 sync

 echo ""
 echo "Création d'une partition et d'un système de fichier en fin de clé destinée à accueillir la persistance"
 echo "start=2GiB" | sfdisk --force --append ${DEVICE}
 mkfs.ext4 -L persistence ${DEVICE}2

 echo ""
 echo "Vérification des deux partitions de la clé ${DEVICE}"
 lsblk ${DEVICE}

 echo ""
 echo "Montage de la partition persistence"
 mount -t ext4 ${DEVICE}2 $TEMPDIR

 echo ""
 echo "Paramétrage de la partition persistence"
 echo "/ union" > $TEMPDIR/persistence.conf

 echo "Démontage de la partition persistence"
 umount $TEMPDIR

 echo ""
 echo "Démontage de la clé ${DEVICE}"
 umount  ${DEVICE}*

fi

echo "TERMINE"
