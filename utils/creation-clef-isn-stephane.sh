echo "CREATION DE LA CLE LIVE - PERSISTANTE"
echo "-------------------------------------"
echo ""

echo "Vérification: vérifiez bien que l'argument correspond bien à la clé choisie"
echo ""
lsblk

echo ""
read -p  "Quelle  clé faut il effacer  :" cle
read -p  "La clé $cle va être effacée. Tout est OK ? oui/non :" reponse





if [ $reponse = 'oui' ]; then
 echo "Démontage de la clé $1"
 umount  /dev/${cle}*

 sleep 2s
 echo ""
 echo "Création de la clé $cle"
 parted   /dev/$cle    mklabel msdos
 sleep 2s
 parted -a optimal  /dev/$cle  mkpart primary fat32 0% 2048MB
 sleep 1s
 #parted -a optimal  /dev/$cle  mkpart primary ext4  2048MB 100%
 sleep 1s
 mkfs.vfat -n USBLIVE /dev/${cle}1
 #mkfs.ext4 -L persistence /dev/${cle}2


 echo ""
 echo "Copie de l'iso sur la clé $cle"
 dd if=liveISN-debian-amd64.hybrid.iso of=/dev/$cle bs=4M status=progress && sync



 echo ""
 echo "Création d' une partition en fin de clé destinée à accueillir la persistance"
 
 parted -a optimal  /dev/$cle  mkpart primary ext4  2048MB 100%
 mkfs.ext4 -L persistence /dev/${cle}2

 echo ""
 echo "Vérification des deux partitions de la clé $cle"
 lsblk


 echo ""
 echo "Montage de la partition persistence"
 mkdir /mnt/clelive
 mount -t ext4 /dev/${cle}2 /mnt/clelive


 echo ""
 echo "Paramétrage de la partition persistence"
 echo "/ union" > /mnt/clelive/persistence.conf

 echo "Démontage de la partition persistence"
 umount /mnt/clelive
 rm -r /mnt/clelive

 echo ""
 echo "Démontage de la clé $cle"
 umount  /dev/${cle}*

fi

echo "TERMINE"
