#!/bin/bash

set -e

PROCESSING=processing-3.5.3
ARCHIVE=${PROCESSING}-linux64.tgz

DESTDIR=/opt/processing
DESTBIN=/usr/bin/processing

# Désinstallation (partielle) de l'ancienne version
rm -fr $DESTDIR
rm -f $DESTBIN

# Téléchargement et décompression
cd /tmp
rm -f processing.tgz
wget -O processing.tgz -e use_proxy=yes -e http_proxy=172.16.0.1:3128 -e https_proxy=172.16.0.1:3128 --no-check-certificate http://download.processing.org/${ARCHIVE}
tar -xf processing.tgz

## Installation des fichiers
mv $PROCESSING $DESTDIR
ln -s $DESTDIR/processing $DESTBIN

################################################################################
# Installation de l'application de bureau

SCRIPT_PATH=$DESTDIR
RESOURCE_NAME=processing-pde
TMP_DIR=`mktemp --directory`

# Création des fichiers .desktop et .xml (copiés depuis l'installateur de processing)
cat <<EOF > ${TMP_DIR}/${RESOURCE_NAME}.desktop
[Desktop Entry]
Type=Application
Name=Processing IDE
GenericName=Processing IDE
Comment=Open-source software prototyping platform
Comment[fr]=Logiciel libre de programmation orientée multimédia
Exec=processing
Icon=${RESOURCE_NAME}
Terminal=false
Categories=Development;IDE;Programming;
MimeType=text/x-processing;
Keywords=sketching;software;animation;programming;coding;
StartupWMClass=processing-app-Base
EOF

cat <<EOF > ${TMP_DIR}/${RESOURCE_NAME}.xml
<?xml version='1.0' encoding='utf-8'?>
<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
    <mime-type type="text/x-processing">
        <comment>Processing source code</comment>
        <comment xml:lang="fr">code source Processing</comment>
        <sub-class-of type="text/x-csrc"/>
        <glob pattern="*.pde"/>
    </mime-type>
</mime-info>
EOF

# Copiée depuis l'installateur de processing `install.sh`
# Install the icon files using name and resolutions
xdg-icon-resource install --context mimetypes --size 16 "${SCRIPT_PATH}/lib/icons/pde-16.png" $RESOURCE_NAME
xdg-icon-resource install --context mimetypes --size 32 "${SCRIPT_PATH}/lib/icons/pde-32.png" $RESOURCE_NAME
xdg-icon-resource install --context mimetypes --size 48 "${SCRIPT_PATH}/lib/icons/pde-48.png" $RESOURCE_NAME
xdg-icon-resource install --context mimetypes --size 64 "${SCRIPT_PATH}/lib/icons/pde-64.png" $RESOURCE_NAME
xdg-icon-resource install --context mimetypes --size 128 "${SCRIPT_PATH}/lib/icons/pde-128.png" $RESOURCE_NAME
xdg-icon-resource install --context mimetypes --size 256 "${SCRIPT_PATH}/lib/icons/pde-256.png" $RESOURCE_NAME
xdg-icon-resource install --context mimetypes --size 512 "${SCRIPT_PATH}/lib/icons/pde-512.png" $RESOURCE_NAME
xdg-icon-resource install --context mimetypes --size 1024 "${SCRIPT_PATH}/lib/icons/pde-1024.png" $RESOURCE_NAME

# Install the created *.desktop file
xdg-desktop-menu install "${TMP_DIR}/${RESOURCE_NAME}.desktop"

# Create icon on the desktop
xdg-desktop-icon install "${TMP_DIR}/${RESOURCE_NAME}.desktop"

# Install Processing mime type
xdg-mime install "${TMP_DIR}/${RESOURCE_NAME}.xml"

# Install icons for mime type
xdg-icon-resource install --context mimetypes --size 16 "${SCRIPT_PATH}/lib/icons/pde-16.png" text-x-processing
xdg-icon-resource install --context mimetypes --size 32 "${SCRIPT_PATH}/lib/icons/pde-32.png" text-x-processing
xdg-icon-resource install --context mimetypes --size 48 "${SCRIPT_PATH}/lib/icons/pde-48.png" text-x-processing
xdg-icon-resource install --context mimetypes --size 64 "${SCRIPT_PATH}/lib/icons/pde-64.png" text-x-processing
xdg-icon-resource install --context mimetypes --size 128 "${SCRIPT_PATH}/lib/icons/pde-128.png" text-x-processing
xdg-icon-resource install --context mimetypes --size 256 "${SCRIPT_PATH}/lib/icons/pde-256.png" text-x-processing
xdg-icon-resource install --context mimetypes --size 512 "${SCRIPT_PATH}/lib/icons/pde-512.png" text-x-processing
xdg-icon-resource install --context mimetypes --size 1024 "${SCRIPT_PATH}/lib/icons/pde-1024.png" text-x-processing

# Make the Processing Development Environment the default app for *.pde files
xdg-mime default ${RESOURCE_NAME}.desktop text/x-processing
