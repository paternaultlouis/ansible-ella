#!/bin/bash

set -e
cd /tmp

wget -O geogebra.deb "http://www.geogebra.org/download/deb.php?arch=amd64"
dpkg -i geogebra.deb
