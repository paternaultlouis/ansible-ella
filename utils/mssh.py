#!/usr/bin/env python3

# Copyright 2018 Louis Paternault
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess

from ansible.inventory.manager import InventoryManager
from ansible.parsing.dataloader import DataLoader

HOSTFILE = os.path.join(
    os.path.dirname(__file__),
    os.path.pardir,
    "hosts",
    )

if __name__ == "__main__":
    inventory = InventoryManager(
        loader = DataLoader(),
        sources = [HOSTFILE],
    )
    addresses = [host.address for host in inventory.get_hosts()]
    subprocess.run(["mssh"] + addresses)
