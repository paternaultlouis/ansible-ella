⚠️ *Attention !*  À notre grand regret, nous avons perdu notre salle auto-administrée sous GNU/Linux. Ce dépôt n'est donc plus utilisé, et deviendra probablement rapidement obsolète (été 2019).

# Installation des machines

- À partir d'une debian stretch (la version [netinst](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.4.0-amd64-netinst.iso) suffit, installer à partir d'une autre version n'a aucune importance).

- Lors de l'installation, utiliser ces éléments de configuration.

  - Langue, clavier, timezone: France
  - Nom de la machine: `poste309-XX` (remplacer `XX` par le numéro de la machine).
  - Domaine: *vide*
  - Mot de passe root: *vide*
  - Utilisateur : `prof`
  - Mot de passe : `****`
  - Partition des disques : *par défaut*, avec `/home` séparé.
  - Proxy apt : *vide*
  - Sélection des logiciels : Seulement `serveur SSH` et `utilitaires usuels du système`.
  - Proxy pour la connection à internet, mettre l'URL du proxy.

- Une fois redémarré, installer `avahi-daemon` :

    sudo apt install avahi-daemon

# Installation du poste `prof`

- Installer `git`, `ansible` :

        sudo apt install git ansible

- Configurer git pour utiliser le proxy, clôner le dépôt :

        git config --global http.proxy URL-DU-PROXY
        git clone https://forge.apps.education.fr/paternaultlouis/ansible-ella.git

- Générer une clef ssh

        ssh-keygen -b 4096

# Sur `prof`, à chaque ajout/remplacement d'une machine

- Copier la clef ssh sur toutes les machines

        for machine in prof $(seq -w 25)
        do
          ssh-copy-id -i ~/.ssh/id_rsa.pub prof@poste309-${machine}
          ssh-copy-id -i ~/.ssh/id_rsa.pub prof@poste309-${machine}.local
        done

- Lancer ansible

        ansible-playbook -i hosts playbook.yml --extra-vars "ansible_become_pass=XXX"

# Ouvrir un shell sur toutes les machines avec `mssh`

    ./utils/mssh.py
